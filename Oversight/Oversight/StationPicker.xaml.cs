﻿using Oversight.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Oversight
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StationPicker : ContentPage
	{
        public string TripName { get; private set; }

		public StationPicker()
		{
			InitializeComponent ();

            TripName = Global.CurrTrip.Name;

            int i = 0;
            StationListView.ItemsSource = Global.CurrTrip.Stations.Select((p) => new ListItemTemplate(p.Name, ++i));

            BindingContext = this;
		}

        private async void StationListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Global.CurrStation = Global.CurrTrip.Stations.Find((p) => p.Name == (e.Item as ListItemTemplate).Name);

            await Navigation.PushAsync(new StationPage());

            ((ListView)sender).SelectedItem = null;
        }
    }
}