﻿using Oversight.Languages;
using System;
using System.Collections.Generic;
using System.Text;

namespace Oversight.Classes
{
    public static class Global
    {
        public static Language Language;

        public static Trip CurrTrip;

        public static Station CurrStation;
    }
}
