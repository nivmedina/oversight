﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Text;

namespace Oversight.Classes
{
    static class Utility
    {
        public static List<string> ReadEmbeddedFileLines(string fullFileName)
        {
            List<string> list = new List<string>();
            string line = string.Empty;
            using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(fullFileName))
            using (StreamReader reader = new StreamReader(stream))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    list.Add(line);
                }
            }
            return list;
        }

        public static Dictionary<string, string> ReadResourceFile(string fullResourceFileName, Assembly assembly)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            ResourceManager rm = new ResourceManager(fullResourceFileName, assembly);
            ResourceSet rs = rm.GetResourceSet(CultureInfo.CurrentCulture, true, true);
            foreach (DictionaryEntry entry in rs) dic.Add(entry.Key.ToString(), entry.Value.ToString());
            return dic;
        }
    }
}
