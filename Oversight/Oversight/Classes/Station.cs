﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Oversight.Classes
{
    public class Station
    {
        public string Name { get; set; }

        public string ID { get; set; }

        public List<Station> ChildStations { get; set; }

        public Station ParentStation { get; set; }

        public string Description = string.Empty;

        public bool HasDescription { get; set; } = false;

        public bool HasImage { get; set; } = false;

        public bool HasAudio { get; set; } = false;

        public Station(string id)
        {
            ID = id;
            ChildStations = new List<Station>();
        }

        public Station(string id, Station child)
        {
            ID = id;
            ChildStations = new List<Station>
            {
                child
            };
        }

        public static List<Station> GetAvailableStations(string name)
        {
            List<Station> list = new List<Station>();

            var stations = Utility.ReadResourceFile("Oversight.Stations", typeof(Station).Assembly).Where((p) => p.Key.StartsWith(name));

            foreach(var station in stations)
            {
                string stationID = station.Key.Replace(name + "_", "");
                Station curr = list.Find((p) => p.ID == stationID) ?? new Station(stationID);
                string[] values = station.Value.Split('|');
                if(!string.IsNullOrEmpty(values[0]))
                {
                    Station parent = list.Find((p) => p.ID.Equals(values[0]));
                    if (parent == null)
                    {
                        parent = new Station(values[0], curr);
                        list.Add(parent);
                    }
                    else
                    {
                        parent.ChildStations.Add(curr);
                    }
                    curr.ParentStation = parent;
                    list.Remove(curr);
                }
                else
                {
                    if(!list.Contains(curr)) list.Add(curr);
                }
                if (Convert.ToBoolean(values[1])) curr.HasDescription = true;
                if (Convert.ToBoolean(values[2])) curr.HasImage = true;
                if (Convert.ToBoolean(values[3])) curr.HasAudio = true;
            }
            // Localization
            foreach(var station in list) LocalizeStations(station, name);
            return list;
        }

        public static void LocalizeStations(Station station, string name)
        {
            var localizedValues = Global.Language.Values["Station_" + station.ID + "_" + name].Split('|');
            station.Name = localizedValues[0];
            if (station.HasDescription) station.Description = localizedValues[1];
            foreach (var child in station.ChildStations) LocalizeStations(child, name);
        }
    }
}
