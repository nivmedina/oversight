﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Oversight.Classes
{
    public class ListItemTemplate
    {
        public string Name { get; set; }

        public int OrderNumber { get; set; }

        public ListItemTemplate(string name, int order)
        {
            Name = name;
            OrderNumber = order;
        }
    }
}
