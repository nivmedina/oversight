﻿using Oversight.Languages;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Oversight.Classes
{
    public class Trip
    {
        public string Name { get; set; }

        public string ID { get; private set; }

        public List<Station> Stations { get; set; }

        public Trip(string id, string name, List<Station> stations)
        {
            ID = id;
            Stations = stations;
            Name = name;
        }

        public static List<string> GetAvailableTrips() => Global.Language.Values.Where((p) => p.Key.StartsWith("Trip")).Select((p) => p.Value).ToList();

        public static void Initialize(string name)
        {
            string tripID = Global.Language.Values.First(p => (p.Value == name)).Key.Replace("Trip_", "");
            Trip trip = new Trip(tripID, name, Station.GetAvailableStations(tripID));

            Global.CurrTrip = trip;
        }
    }
}