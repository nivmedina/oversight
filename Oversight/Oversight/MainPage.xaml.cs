﻿using Oversight.Classes;
using Oversight.Languages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Oversight
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();

            int i = 0;
            LangListView.ItemsSource = Language.GetAvailableLanguages().Select((p) => new ListItemTemplate(p, ++i)); ;

            BindingContext = this;
        }

        private async void LangListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selected = e.Item as ListItemTemplate;

            Global.Language = Language.Initialize(selected.Name);

            await Navigation.PushAsync(new TripPicker());

            ((ListView)sender).SelectedItem = null;
        }
    }
}
