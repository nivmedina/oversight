﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Oversight.Classes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.SimpleAudioPlayer;

namespace Oversight
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class StationPage : ContentPage
	{
        public string Name { get; set; }

		public StationPage ()
		{
			InitializeComponent ();

            Station curr = Global.CurrStation;

            Name = curr.Name;

            if (curr.HasDescription)
            {
                DescriptionView.Text = curr.Description;
                DescriptionView.IsVisible = true;
            }
            if(curr.HasImage)
            {
                ImageView.Source = ImageSource.FromResource("Oversight.Images." + curr.ID + ".jpg", typeof(StationPage).Assembly);
                ImageView.IsVisible = true;
            }
            if(curr.HasAudio)
            {
                AudioButtonView.IsVisible = true;
            }
            if(curr.ChildStations.Count > 0)
            {
                int i = 0;
                ChildStationListView.ItemsSource = curr.ChildStations.Select((p) => new ListItemTemplate(p.Name, ++i));
                ChildStationListView.IsVisible = true;
            }
            BindingContext = this;
		}

        private async void ChildStationListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Global.CurrStation = Global.CurrStation.ChildStations.Find((p) => p.Name == (e.Item as ListItemTemplate).Name);

            await Navigation.PushAsync(new StationPage());

            ((ListView)sender).SelectedItem = null;
        }

        private void AudioButtonView_Clicked(object sender, EventArgs e)
        {
            ISimpleAudioPlayer player = CrossSimpleAudioPlayer.Current;
            player.Load(typeof(StationPage).Assembly.GetManifestResourceStream("Oversight.Audio." + Global.CurrStation.ID + ".mp3"));
            player.Play();
        }

        protected override bool OnBackButtonPressed()
        {
            if(Global.CurrStation != null && Global.CurrStation.ParentStation != null)
            {
                Global.CurrStation = Global.CurrStation.ParentStation;
            }
            return base.OnBackButtonPressed();
        }
    }
}