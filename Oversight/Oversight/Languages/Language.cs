﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Resources;
using Oversight.Classes;
using System.Text;

namespace Oversight.Languages
{
    public class Language
    {
        public string Name { get; private set; }

        public Dictionary<string, string> Values { get; private set; }

        private Language(string name, Dictionary<string, string> values)
        {
            name = Name;
            Values = values;
        }

        public static Language Initialize(string name) => new Language(name, Utility.ReadResourceFile("Oversight.Languages." + name, typeof(Language).Assembly));

        public static List<string> GetAvailableLanguages() => Utility.ReadEmbeddedFileLines("Oversight.Languages.Languages.txt");
    }
}
