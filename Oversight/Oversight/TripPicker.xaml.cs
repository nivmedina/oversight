﻿using Oversight.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Oversight
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TripPicker : ContentPage
	{
        public string WelcomeMessage { get; set; } = Global.Language.Values["General_Welcome_Message"];

		public TripPicker ()
		{
			InitializeComponent ();

            int i = 0;
            TripListView.ItemsSource = Trip.GetAvailableTrips().Select((p) => new ListItemTemplate(p, ++i));

            BindingContext = this;
		}

        private async void TripListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var selected = e.Item as ListItemTemplate;

            Trip.Initialize(selected.Name);

            await Navigation.PushAsync(new StationPicker());

            ((ListView)sender).SelectedItem = null;
        }
    }
}